﻿using InventarioDB.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Interface
{
	/// <summary>
	/// Interaction logic for Window1.xaml
	/// </summary>
	public partial class Window1 : Window
	{
		private List<ItemInventario> GeneralProductList = new List<ItemInventario>();

		public Window1()
		{
			InitializeComponent();
		}

		private void Window_GotFocus( object sender, RoutedEventArgs e )
		{

		}

		private void Window_Loaded( object sender, RoutedEventArgs e )
		{
			using ( var context = new InventarioContext() )
			{
				context.Configuration.LazyLoadingEnabled = false;
				List<ItemInventario> products;
				products = context.Items.ToList();
				FillGrid( products );
			}
		}

		/// <summary>
		/// Fills the grid view with Items
		/// </summary>
		/// <param name="Search"></param>
		//TASK pass a List<ItemInventario> as argument to perform custom searches
		private void FillGrid( List<ItemInventario> products)
		{
			ProductList.ItemsSource = products;
			ProductList.Columns.Remove( ProductList.Columns.First( cl => cl.Header.ToString() == "Existencias" ) );
			ProductList.Columns [ 0 ].Width = 60;
		}

		private void SearchTxt_TextChanged( object sender, TextChangedEventArgs e )
		{
			string Search = SearchTxt.Text;
			using ( var context = new InventarioContext() )
			{
				context.Configuration.LazyLoadingEnabled = false;

				List<ItemInventario> products;
				if ( !string.IsNullOrEmpty( Search ) )
				{
					products = context.Items.Where( it => it.Descripcion.Contains( Search ) ).ToList();
				}
				else
					products = context.Items.ToList();

				FillGrid( products );
			}

		}
	}
}
