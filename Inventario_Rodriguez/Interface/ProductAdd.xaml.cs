﻿using InventarioDB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Interface
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void SaveClick( object sender, RoutedEventArgs e )
		{
			using ( var context = new InventarioContext() )
			{
				context.AddItemOrExistences( 
					code.Text, 
					int.Parse( quantity.Text ), 
					description.Text, 
					decimal.Parse( cost.Text ), 
					decimal.Parse( sale_price.Text ));
				context.SaveChanges();
			}
			MessageBox.Show( "Se ha guardado el producto y existencias", "Inventario", MessageBoxButton.OK, MessageBoxImage.Asterisk );
			CleanForm();
		}

		private void Clean_Click( object sender, RoutedEventArgs e )
		{
			CleanForm();
		}

		private void CleanForm()
		{
			code.Clear();
			quantity.Clear();
			description.Clear();
			cost.Clear();
			sale_price.Clear();
		}

	}
}
