﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace InventarioDB.Model
{
    public class InventarioContext : DbContext
    {
        public virtual DbSet<ItemInventario> Items { get; set; }
        public virtual DbSet<ItemExistente> Existencias { get; set; }

        protected override void OnModelCreating( DbModelBuilder modelBuilder )
        {
            modelBuilder.Entity<ItemInventario>().Property( a => a.Costo ).HasPrecision( 18, 02 );
            modelBuilder.Entity<ItemInventario>().Property( a => a.Precio ).HasPrecision( 18, 02 );

            base.OnModelCreating( modelBuilder );
        }

		public void AddItemOrExistences( string code, int quantity, string description, decimal cost, decimal sale_price )
		{
			//task encapsulate the whole method...
			/* 1. Verificar que el item no existe, de lo contrario agregar existencias**/
			if ( !this.Items.Any( a => a.Codigo == code ) )
			{
				//Add new item
				ItemInventario additem = new ItemInventario()
				{
					Codigo = code,
					Descripcion = description,
					Costo = cost,
					Precio = sale_price
				};
				//save the item first
				this.Items.Add( additem );
				this.SaveChanges();

				//save the existences
				additem.AddExistencias( quantity );
				this.SaveChanges();
			}
			else
			{
				//Add Existencias
				//get Item
				var Existent_Item = this.Items.FirstOrDefault( x => x.Codigo == code );
				Existent_Item.AddExistencias( quantity );
			}
		}
	}
}
