﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventarioDB.Model.Common
{
    public enum Estado
    {
        Disponible,
        Vendido,
        Retirado
    }
}
