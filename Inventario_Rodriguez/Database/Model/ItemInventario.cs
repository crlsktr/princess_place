﻿using InventarioDB.Common.Model;
using InventarioDB.Model.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
namespace InventarioDB.Model
{
    public class ItemInventario
    {
        [Key]
        public string Codigo {get;set;}
        public string Descripcion { get; set; }
        public decimal Costo { get; set; }
        public decimal Precio { get; set; }
        public virtual List<ItemExistente> Existencias { get; set; }

        public int Existencia { get {
            if (Existencias == null)
                return 0;
            else
                return Existencias.Count(a => a.EstadoItemExistente == Common.Estado.Disponible); //only count units where Estado == Disponible
        } }

        public void AddExistencias(int Quantity)
        {
            if (Existencias == null)
            {
                Existencias = new List<ItemExistente>();
            }

            for ( int i=0; i<Quantity; i++ )
            {
                Existencias.Add( new ItemExistente()
                {
					Codigo = Guid.NewGuid().ToString(),
                    EstadoItemExistente = Estado.Disponible,
                    FechaIngreso = DateTime.Now
                } );
            }
        }
    }
}