namespace InventarioDB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PrecioCosto : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ItemInventarios", "Costo", c => c.Decimal(nullable: false, precision: 2, scale: 2));
            AddColumn("dbo.ItemInventarios", "Precio", c => c.Decimal(nullable: false, precision: 2, scale: 2));
            AddColumn("dbo.ItemInventarios", "Tipo", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ItemInventarios", "Tipo");
            DropColumn("dbo.ItemInventarios", "Precio");
            DropColumn("dbo.ItemInventarios", "Costo");
        }
    }
}
