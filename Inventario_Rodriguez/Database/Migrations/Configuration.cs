namespace InventarioDB.Migrations
{
    using InventarioDB.Model;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<InventarioDB.Model.InventarioContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(InventarioDB.Model.InventarioContext context)
        {
            //  This method will be called after migrating to the latest version.
            //context.Items.Add(new Model.ItemInventario
            //{
            //    Codigo = "3232",
            //    Descripcion = "laropa",
            //    Existencia = 1
            //});
            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
