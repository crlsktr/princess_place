namespace InventarioDB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ItemInventarios",
                c => new
                    {
                        codigo = c.String(nullable: false, maxLength: 128),
                        descripcion = c.String(),
                        existencia = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.codigo);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ItemInventarios");
        }
    }
}
