namespace InventarioDB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Existencias : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ItemInventarios", "ItemExistenteID", c => c.Guid(identity: true));
            AddColumn("dbo.ItemInventarios", "FechaIngreso", c => c.DateTime());
            AddColumn("dbo.ItemInventarios", "FechaEgreso", c => c.DateTime());
            AddColumn("dbo.ItemInventarios", "EstadoItemExistente", c => c.Int());
            AddColumn("dbo.ItemInventarios", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.ItemInventarios", "ItemInventario_Codigo", c => c.String(maxLength: 128));
            CreateIndex("dbo.ItemInventarios", "ItemInventario_Codigo");
            AddForeignKey("dbo.ItemInventarios", "ItemInventario_Codigo", "dbo.ItemInventarios", "Codigo");
            DropColumn("dbo.ItemInventarios", "Existencia");
            DropColumn("dbo.ItemInventarios", "Tipo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ItemInventarios", "Tipo", c => c.Int(nullable: false));
            AddColumn("dbo.ItemInventarios", "Existencia", c => c.Int(nullable: false));
            DropForeignKey("dbo.ItemInventarios", "ItemInventario_Codigo", "dbo.ItemInventarios");
            DropIndex("dbo.ItemInventarios", new[] { "ItemInventario_Codigo" });
            DropColumn("dbo.ItemInventarios", "ItemInventario_Codigo");
            DropColumn("dbo.ItemInventarios", "Discriminator");
            DropColumn("dbo.ItemInventarios", "EstadoItemExistente");
            DropColumn("dbo.ItemInventarios", "FechaEgreso");
            DropColumn("dbo.ItemInventarios", "FechaIngreso");
            DropColumn("dbo.ItemInventarios", "ItemExistenteID");
        }
    }
}
