﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;

namespace InventarioDB
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
			using ( var context = new Model.InventarioContext() )
			{
			//	//context.Items.Add(new Model.ItemInventario()
			//	//    {
			//	//        Codigo = Guid.NewGuid().ToString(),
			//	//        Descripcion = "el nuevo producto",
			//	//        //Existencia = 12
			//	//    });
				foreach ( var item in context.Items.ToList())
				{
					Console.WriteLine(
						"------------------------" +
						"item:\ndescription: " + item.Descripcion + 
						"\ncantidad: " + item.Existencia + 
						"\nprecio: " +item.Precio + 
						"\ncosto: " + item.Costo
						);
				}
			}
			Console.ReadLine();



			///*This lines are the visual interface startup*/
			//Application.EnableVisualStyles();
			//Application.SetCompatibleTextRenderingDefault( false );
			//Application.Run( new Tester() );
        }
    }
}
